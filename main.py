from python import batse

batse_months = batse.read_months()
batse.basschords(batse_months, False)
batse.basschords(batse_months, True)
batse.footsteps(batse_months, False, 64)
batse.footsteps(batse_months, False, 80)
batse.footsteps(batse_months, True, 96)
batse.windarps(batse_months)
batse.ligeti(batse_months)
